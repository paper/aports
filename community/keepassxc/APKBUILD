# Contributor: Leonardo Arena <rnalrd@alpinelinux.org>
# Contributor: August Klein <amatcoder@gmail.com>
# Contributor: Orson Teodoro <orsonteodoro@hotmail.com>
# Contributor: Max Rees <maxcrees@me.com>
# Maintainer: Orson Teodoro <orsonteodoro@hotmail.com>
pkgname=keepassxc
pkgver=2.6.4
pkgrel=0
pkgdesc="Community-driven port of the Windows application Keepass Password Safe"
url="https://keepassxc.org/"
arch="all"
license="GPL-2.0-only OR GPL-3.0-only"
depends="hicolor-icon-theme"
makedepends="argon2-dev cmake libgcrypt-dev libqrencode-dev libsodium-dev
	libxi-dev libxtst-dev qt5-qtbase-dev qt5-qtsvg-dev qt5-qttools-dev
	qt5-qtx11extras-dev zlib-dev quazip-dev yubico-c-dev ykpers-dev
	asciidoctor"
source="https://github.com/keepassxreboot/keepassxc/releases/download/$pkgver/keepassxc-$pkgver-src.tar.xz"
subpackages="$pkgname-doc"
provides="dbus:org.freedesktop.Secrets"
# Smallest priority, unusual setup
provider_priority=5

build() {
	cmake -B build \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=/usr/lib \
		-DCMAKE_BUILD_TYPE=None \
		-DKEEPASSXC_BUILD_TYPE=None \
		-DWITH_XC_AUTOTYPE=ON \
		-DWITH_XC_NETWORKING=ON \
		-DWITH_XC_BROWSER=ON \
		-DWITH_XC_YUBIKEY=ON \
		-DWITH_XC_SSHAGENT=ON \
		-DWITH_XC_KEESHARE=ON \
		-DWITH_XC_UPDATECHECK=OFF \
		-DWITH_XC_FDOSECRETS=ON
	cmake --build build
}
package() {
	DESTDIR="$pkgdir" cmake --build build --target install
	install -d "$pkgdir"/usr/share/doc/$pkgname/
	install -t "$pkgdir"/usr/share/doc/$pkgname/ \
		   "$builddir"/CHANGELOG.md "$builddir"/README.md
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

sha512sums="3bde0c8670ba14be80c6f3676bd447b0855a2af2915a395ee236c2d4c6e4b859936351643d679480aae1fcf55ed4315447ae927ac9bdedeb0332593cb4e9fedb  keepassxc-2.6.4-src.tar.xz"
